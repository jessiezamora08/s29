const express = require ('express')
const app = express ()
const port = 3000;

app.use (express.json())
app.use(express.urlencoded({extended:true}));

app.get ('/home', (req,res) => {
    res.send ('This is Home!!!!')
})

let users = [];

app.get ('/users' , (req,res) => {
    res.send (users)
})

app.post ('/new-user', (req,res) =>{
    if(req.body.firstName !=='' && req.body.lastName !=='' && req.body.email!== '') 
    users.push (req.body)
    res.send (`Hello there ${req.body.firstName}`)
})

app.listen(port, ()=> console.log(`server running at port ${port}`))


